import serial
import datetime
import time
from time import gmtime, strftime
f = open("imu_log3.csv", "w")
def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False
# make sure the 'COM#' is set according the Windows Device Manager
ser = serial.Serial('/dev/ttyACM1', 115200, timeout=1)
time.sleep(2)


while(1):
    line = ser.readline()   # read a byte
    if line:
        string = line.decode()  # convert the byte string to a unicode string
        if(isfloat(string)):
            num = float(string) # convert the unicode string to an int
            # print(num, ',', datetime.datetime.now())
            yeet = "{},{}\n".format(num, datetime.datetime.utcnow()) 
            f.write(yeet)

ser.close()