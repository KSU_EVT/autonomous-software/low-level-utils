"""
Make sure to change the data_path before using

Command to get gps data from .json file:

jq -r 'select(contains({"class":"TPV"})) | [.lat,.lon,.epx,.epy,.eph,.time,.mode] | @csv'  big_test2.json > big_test2.csv

so i dont forget it again lol xd
"""

from cProfile import label
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

def get_sec(time_str):
       h, m, s = time_str.split(':')
       return int(h) * 3600 + int(m) * 60 + int(s)

def update():
       return 0

# plt.style.use('_mpl-gallery')

data_path = 'yaw_test3.csv'
imu_path = 'imu_log3.csv'

# make data:
data = pd.read_csv(data_path, names=['LATITUDE', 'LONGITUDE', 'LAT_ERROR', 'LONG_ERROR', 'HORIZONTAL_ERROR', 'TIME', 'STATUS', 'IMU_RATE'], sep=',')
gps_data = tuple(zip(data['LATITUDE'].values, data['LONGITUDE'].values, data['LAT_ERROR'].values, data['LONG_ERROR'].values, data['HORIZONTAL_ERROR'].values, pd.to_datetime(data['TIME'].values), data['STATUS'].values))
imu = pd.read_csv(imu_path, names=['IMU_RATE', 'IMU_TIME'], sep=',')
imu_data = tuple(zip(imu['IMU_RATE'].values, imu['IMU_TIME'].values))
imu_rate = []
time_imu = []
lat = []
long = []
lat_err = []
long_err = []
h_err = []
time = []
fix = []
x = range(0, len(gps_data), 1)
for d in gps_data:
       lat.append(d[0])
       long.append(d[1])
       lat_err.append(d[2])
       long_err.append(d[3])
       h_err.append(d[4])
       time.append(pd.Timestamp(d[5]).timestamp())
       print(pd.Timestamp(d[5]).timestamp())
       fix.append(d[6])
for d in imu_data:
       imu_rate.append(d[0])
       time_imu.append(pd.Timestamp(d[1]).timestamp())
# plot:
fig, ax = plt.subplots(1, figsize=(10,10))
plt.subplots_adjust(left=0.07, bottom=0.15)
plt.rcParams.update({'figure.autolayout': True})
line_h_err = plt.step(time, h_err, color='green', label='Horizontal Error')
line_fix = plt.step(time, fix, color='purple', label='Fix Status')
line_imu_rate = plt.plot(time_imu, imu_rate, color='orange', label='imu data')

# ax.set(xticks=np.arange(0, len(time)-1, 15))
ax.legend()
ax.set_ylabel('Error')
ax.set_xlabel('Seconds')

plt.show()
