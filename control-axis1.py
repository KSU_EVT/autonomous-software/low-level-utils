"""Simple example showing how to get gamepad events."""

from __future__ import print_function


from inputs import get_gamepad
import time
import odrive
from odrive.enums import *

import math
from odrive.utils import start_liveplotter,dump_errors
deadzone=0
max=255
odrv0 = odrive.find_any()
def main():
    liveplot()
    """Just print out some event infomation when the gamepad is used."""
    
    print(str(odrv0.vbus_voltage))
    print("starting calibration...")
    # odrv0.axis1.requested_state = AXIS_STATE_FULL_CALIBRATION_SEQUENCE
    # while odrv0.axis1.current_state != AXIS_STATE_IDLE:
    #     time.sleep(0.1)
    
    odrv0.axis1.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL
    while 1:
        events = get_gamepad()
        for event in events:
            #print()
            #print(event.code,event.state)
            
            if (event.code=="ABS_X"):
                
                test=abs(((event.state/max)-0.5) * 1 )
                odrv0.axis1.controller.input_torque=test
                print(test)
            if (event.code=="BTN_WEST"):
                if(bool(event.state)):
                    dump_errors(odrv0,True)
                    
                    print(str(odrv0.vbus_voltage))
                    print("starting calibration...")
                    odrv0.axis1.requested_state = AXIS_STATE_FULL_CALIBRATION_SEQUENCE
                    time.sleep(0.2)
                    while odrv0.axis1.current_state != AXIS_STATE_IDLE:
                        time.sleep(0.1)
                    
                    odrv0.axis1.requested_state = AXIS_STATE_CLOSED_LOOP_CONTROL

def liveplot():
    start_liveplotter(lambda: [
    odrv0.axis1.controller.input_torque,
    odrv0.axis1.motor.current_control.Iq_setpoint,
    odrv0.axis1.motor.current_control.Iq_measured])


if __name__ == "__main__":
    main()